#!/usr/bin/env python

from distutils.core import setup

setup(name='RadixCodec',
      version='0.0.1',
      description='Compact binary encoding of strings from a restricted alphabet.',
      author='Hal Blackburn',
      author_email='hal.blackburn@gmail.com',
      url='https://github.com/h4l/Radix-Codec',
      package_dir={'': 'src'},
      packages=[''],
      py_modules=["radixcodec"],
     )
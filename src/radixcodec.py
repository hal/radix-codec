# Copyright (c) 2012, Hal Blackburn <hal.blackburn@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Provides a means of compactly representing (in binary) strings comprised of 
symbols from an alphabet of a fixed size. 

The term string is used in an abstract sense, not necessarily a string object, 
just a sequence of symbols which may or may not represent characters.

For example, if our alphabet contains (only) the chars a-z, we can represent
strings about in about 60% of the space of ascii/utf-8.

@author: Hal Blackburn
"""

import binascii

class RadixCodec(object):
    """
    A compressor/decompressor of 
    """
    
    def __init__(self, language):
        self._language = list(language)
        self._radix = len(self._language) + 1
        assert self._radix > 0, "Language must have > 0 entries"
    
    def encode(self, term, padtomaxlen=False):
        """
        Encodes a string consisting of entries from the language provided to
        the constructor into a compact binary representation.
        
        If padtomaxlen is True, the output is padded with null bytes to match 
        the length of the longest possible encoded string for a term of equal 
        length. For example, encoding "aaaa" with an a-z alphabet results in 
        'O\xd8' (len 2). Whereas, encoding "zzzz" results in '\x08\x1b\xf0' 
        (len 3). If padtomaxlen is True, the result of encoding "aaaa" is 
        '\x00O\xd8' (len 3). 
        
        The null byte does not effect decoding, but can be useful to ensure 
        the encoded value lengths are consistent when working with fixed width
        fields for binary data.
        """
        basen = map(self._ordinalForSymbol, term)
        intValue = self._baseNDecode(basen)
        packed = self._packInteger(intValue)
        if padtomaxlen:
            maxlen = self.max_bytes_to_encode_term_of_length(len(term))
            print maxlen
            delta = maxlen - len(packed)
            packed = ("\x00" * delta) + packed
        return packed
    
    def decode(self, binaryTerm, strOut=True):
        """
        Decodes a binary string created by encode() into a string consisting of
        entries from the language provided to the constructor.
        """
        intValue = self._unpackInteger(binaryTerm)
        if intValue == 0:
            return []
        term = [self._symbolForOrdinal(x) for x in self._baseNEncode(intValue)]
        if strOut: return "".join(term)
        return term
    
    def _baseNDecode(self, val):
        result = 0
        for i, v in enumerate(reversed(val)):
            result = result + (v * (self._radix ** i))
        return result
    
    def _baseNEncode(self, val):
        result = []
        while True:
            result.insert(0, val % self._radix)
            if val < self._radix:
                return result
            val = val / self._radix
    
    def _ordinalForSymbol(self, symbol):
        ordinal = self._language.index(symbol)
        assert ordinal >= 0
        return ordinal + 1
    
    def _symbolForOrdinal(self, ordinal):
        return self._language[ordinal - 1]
    
    @staticmethod
    def _packInteger(integer):
        assert integer >= 0
        # get hex string w/o the 0x prefix or L suffix for big ints
        hexstr = hex(integer)[2:].rstrip("L")
        # Make hexstr's length a multiple of 2 if it's not already
        if len(hexstr) % 2 != 0:
            hexstr = '0' + hexstr
        return binascii.unhexlify(hexstr)
    
    @staticmethod
    def _unpackInteger(bytestr):
        return int(binascii.hexlify(bytestr), 16)
    
    def max_bytes_to_encode_term_of_length(self, n):
        """
        Gets the length of the longest possible sequence of encoded bytes 
        returned by encode() for an input sequence of length n. 
        """
        return self._bytes_needed_to_represent(self._radix ** n)
    
    @staticmethod
    def _bytes_needed_to_represent(val):
        b = 1
        while True:
            if val < 2**(8*b):
                return b
            b = b + 1  
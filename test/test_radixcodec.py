import unittest, radixcodec, random
from binascii import hexlify

class TestRadixCodecRoundTrip(unittest.TestCase):
    
    LENGTHS = [0, 1, 2, 3, 5, 9, 12, 22, 100, 2000]
    ALPHABETS = [
        "abcdefghijklmnopqrstuvwxyz",
        ["x"],
        [True, False, None],
        ["foo", "bar", "baz"]
    ]

    def setUp(self):
        self._rand = random.Random()
        self._rand.seed(a=0xCAFEBABE)
    
    def testPermutations(self):
        for length in self.LENGTHS:
            for alphabet in self.ALPHABETS:
                for i in range(10):
                    self._roundtrip(length, alphabet)
    
    def _roundtrip(self, length, alphabet):
        codec = radixcodec.RadixCodec(alphabet)
        symbols = [self._rand.choice(alphabet) for x in range(length)]
        
        encoded = codec.encode(symbols)
        decoded = codec.decode(encoded, strOut=False)
        
        print "roundtrip len: {0} alphabet: {1} symbols: {2}, encoded: 0x{3} "\
            "decoded: {4}".format(length, alphabet, symbols, hexlify(encoded), decoded)
        self.assertEqual(symbols, decoded)

if __name__ == "__main__":
    unittest.main()